
# Movie Characters API

Create an Entity Framework Code First workflow and an ASP.NET Core Web API in C#.

## Tools
- .NET 6
- Visual Studio 2022
- SQL Server Management Studio

## Contributors
- [Scott Sonen](https://gitlab.com/scottrs)
- Glaysa Fernandez