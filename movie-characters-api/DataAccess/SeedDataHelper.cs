﻿using movie_characters_api.Models;

namespace movie_characters_api.DataAccess
{
    // This class is responsible for seeding data into the database during migration.
    public class SeedDataHelper
    {
        public static ICollection<Character> GetCharacters()
        {
            return new List<Character>() {
                new Character() {
                    Id = 1,
                    Name = "Steve Rogers",
                    Alias = "Captain America",
                    Gender = "Male",
                    Picture = @"https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2022%2F06%2Fchris-evans-return-as-captain-america-0.jpg?fit=max&cbr=1&q=90&w=750&h=500"
                },
                new Character() {
                    Id = 2,
                    Name = "Tony Stark",
                    Alias = "Iron Man",
                    Gender = "Male",
                    Picture = @"https://is4-ssl.mzstatic.com/image/thumb/zV1ELKfWVxoIo4TZdk8WcA/1200x675mf.jpg"
                },
                new Character() {
                    Id = 3,
                    Name = "Bruce Banner",
                    Alias = "Hulk",
                    Gender = "Male",
                    Picture = @"https://p3.no/filmpolitiet/wp-content/uploads/2010/05/hulk.jpg"
                },
                new Character() {
                    Id = 4,
                    Name = "Anakin Skywalker",
                    Alias = "Darth Vader",
                    Gender = "Male",
                    Picture = @"https://upload.wikimedia.org/wikipedia/commons/3/32/Star_Wars_-_Darth_Vader.jpg"
                }
            };
        }

        public static ICollection<Movie> GetMovies()
        {
            return new List<Movie>() {
                new Movie() {
                    Id = 1,
                    Title = "Avengers: Infinity War",
                    Genre = "Action",
                    ReleaseYear = 2018,
                    Director = "Russo brothers",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/4/4d/Avengers_Infinity_War_poster.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=6ZfuNTqbHE8&ab_channel=MarvelEntertainment",
                    FranchiseId = 1,
                },

                new Movie() {
                    Id = 2,
                    Title = "Avengers: Endgame",
                    Genre = "Action",
                    ReleaseYear = 2019,
                    Director = "Russo brothers",
                    Picture = @"https://lumiere-a.akamaihd.net/v1/images/p_avengersendgame_19751_e14a0104.jpeg?region=0%2C0%2C540%2C810",
                    Trailer = @"https://www.youtube.com/watch?v=TcMBFSGVi1c&ab_channel=MarvelEntertainment",
                    FranchiseId = 1,
                },
                new Movie() {
                    Id = 3,
                    Title = "Star Wars: A New Hope",
                    Genre = "Science Fiction",
                    ReleaseYear = 1977,
                    Director = "George Lucas",
                    Picture = @"https://kbimages1-a.akamaihd.net/ea6a1631-34e8-4369-b777-cf342521d3e0/1200/1200/False/a-new-hope-star-wars-episode-iv.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=TzSm9dWso1o&ab_channel=20thCenturyStudiosUK",
                    FranchiseId = 2,
                }
            };
        }

        public static ICollection<Franchise> GetFranchises()
        {
            return new List<Franchise>()
            {
                new Franchise()
                {
                    Id = 1,
                    Name = "Avengers",
                    Description = "Super hero franchise"
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Star Wars",
                    Description = "Science fiction franchise"
                }
            };
        }
    }
}
