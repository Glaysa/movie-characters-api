﻿using System.ComponentModel.DataAnnotations;

namespace movie_characters_api.Models
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(64)]
        public string? Alias { get; set; }
        [MaxLength(16)]
        public string Gender { get; set; }
        [MaxLength(255)]
        public string? Picture { get; set; }

        // Navigation props
        public ICollection<Movie>? Movies { get; set; }
    }
}
