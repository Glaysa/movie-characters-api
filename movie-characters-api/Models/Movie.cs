﻿using System.ComponentModel.DataAnnotations;

namespace movie_characters_api.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(64)]
        public string Title { get; set; }
        [MaxLength(64)]
        public string Genre { get; set; }
        public int? ReleaseYear { get; set; }
        [MaxLength(64)]
        public string? Director { get; set; }
        [MaxLength(255)]
        public string? Picture { get; set; }
        [MaxLength(255)]
        public string? Trailer { get; set; }

        // Navigation props
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
        public ICollection<Character>? Characters { get; set; }
    }
}
