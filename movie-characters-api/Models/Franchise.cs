﻿using System.ComponentModel.DataAnnotations;

namespace movie_characters_api.Models
{
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string? Description { get; set; }

        // Navigation props
        public ICollection<Movie>? Movies { get; set; }
    }
}
