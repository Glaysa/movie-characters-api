﻿using Microsoft.EntityFrameworkCore;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieCharactersDbContext _context;

        public MovieRepository(MovieCharactersDbContext dbContext)
        {
            _context = dbContext;
        }

        /// <summary>
        /// Gets all movies from database.
        /// </summary>
        /// <returns>All movies from Movies table in database <see cref="List{Movie}" /></returns>
        public async Task<IEnumerable<Movie>> GetMovies()
        {
            return await _context.Movies.ToListAsync();
        }

        /// <summary>
        /// Gets all characters from specific movie by id.
        /// </summary>
        /// <param name="id">Id of the specific movie to check</param>
        /// <returns>
        /// If the movie was not found, <see langword="null"/> is returned.
        /// If the movie was found, the <see cref="List" /> of characters is returned.
        /// </returns>
        public async Task<IEnumerable<Character>?> GetCharactersInMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null) return null;
            return await _context.Movies
                .Where(movie => movie.Id == id)
                .SelectMany(movie => movie.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Gets specific movie by id.
        /// </summary>
        /// <param name="id">Id of the specific movie</param>
        /// <returns>
        /// If the movie was not found, <see langword="null"/> is returned.
        /// If the movie was found, the <see cref="Movie"/> object is returned.
        /// </returns>
        public async Task<Movie?> GetMovieById(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        /// <summary>
        /// Posts a movie to the database.
        /// </summary>
        /// <param name="movie">Movie to be added</param>
        /// <returns>
        /// If the post failed, <see langword="False"/> is returned.
        /// If the movie post succeeded, <see langword="True"/> is returned.
        /// </returns>
        public async Task<bool> PostMovie(Movie movie)
        {
            await _context.Movies.AddAsync(movie);
            int affectedRows = await _context.SaveChangesAsync();
            return affectedRows == 1;
        }

        /// <summary>
        /// Updates a movie in the database with new values.
        /// </summary>
        /// <param name="id">Id of the movie to be updated</param>
        /// <param name="newMovie">DTO of the movie object with new values</param>
        /// <returns>
        /// If the movie was not found or the update failed, <see langword="False"/> is returned.
        /// If the movie was updated, <see langword="True"/> is returned.
        /// </returns>
        public async Task<bool> PutMovie(int id, MovieDTO newMovie)
        {
            var movieFromDb = await _context.Movies.FindAsync(id);
            if (movieFromDb == null) return false;

            _context.Entry(movieFromDb).CurrentValues.SetValues(newMovie);
            int affectedRows = await _context.SaveChangesAsync();

            return affectedRows == 1;
        }

        /// <summary>
        /// Updates the characters list of a movie.
        /// </summary>
        /// <param name="id">Id of the movie to be updated</param>
        /// <param name="characterIds">List of Ids of the characters to be added to the list</param>
        /// <returns>
        /// If the movie was not found, <see langword="null"/> is returned.
        /// If a character's Id is not present in the database, <see langword="null"/> is returned.
        /// If all characters are present in the database, the <see cref="Movie"/> with an updated characters list is returned.
        /// </returns>
        public async Task<Movie?> PutMovieCharacters(int id, int[] characterIds)
        {
            var movie = await _context.Movies.Include(m => m.Characters).Where(m => m.Id == id).FirstOrDefaultAsync();
            if (movie == null) return null;
            var characterList = movie.Characters;

            for (int i = 0; i < characterIds.Length; i++)
            {
                var character = await _context.Characters.FindAsync(characterIds[i]);

                if (character == null)
                {
                    return null;
                }
                else if (character != null && !characterList.Contains(character))
                {
                    characterList.Add(character);
                }
            }
            return movie;
        }

        /// <summary>
        /// Deletes and removes a movie from the database.
        /// </summary>
        /// <param name="id">Id of the movie to be deleted</param>
        /// <returns>
        /// If the movie was not found, <see langword="False"/> is returned.
        /// If the movie was deleted, <see langword="True"/> is returned.
        /// </returns>
        public async Task<bool> DeleteMovie(int id)
        {
            var movieFromDb = await _context.Movies.FindAsync(id);

            if (movieFromDb == null) return false;

            _context.Movies.Remove(movieFromDb);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
