﻿using Microsoft.AspNetCore.Mvc;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Repositories
{
    public interface IMovieRepository
    {
        Task<IEnumerable<Movie>> GetMovies();
        Task<IEnumerable<Character>?> GetCharactersInMovie(int id);
        Task<Movie?> GetMovieById(int id);
        Task<bool> PostMovie(Movie movie);
        Task<bool> PutMovie(int id, MovieDTO newMovie);
        Task<Movie?> PutMovieCharacters(int id, int[] characterIds);
        Task<bool> DeleteMovie(int id);
    }
}
