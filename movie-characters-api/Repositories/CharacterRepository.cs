﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MovieCharactersDbContext _context;

        public CharacterRepository(MovieCharactersDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all characters from database.
        /// </summary>
        /// <returns>All characters from Characters table in database <see cref="List{Character}" /></returns>
        public async Task<IEnumerable<Character>> GetCharacters()
        {
            return await _context.Characters.ToListAsync();
        }

        /// <summary>
        /// Gets specific character by id.
        /// </summary>
        /// <param name="id">Id of the specific character</param>
        /// <returns>
        /// If the character was not found, <see langword="null"/> is returned.
        /// If the character was found, the <see cref="Character"/> object is returned.
        /// </returns>
        public async Task<Character>? GetCharacterById(int id)
        {
            return await _context.Characters.FindAsync(id);
        }

        /// <summary>
        /// Updates a character in the database with new values.
        /// </summary>
        /// <param name="id">Id of the character to be updated</param>
        /// <param name="characterDTO">DTO of the character object with new values</param>
        /// <returns>
        /// If the character was not found or the update failed, <see langword="False"/> is returned.
        /// If the character was updated, <see langword="True"/> is returned.
        /// </returns>
        public async Task<bool> PutCharacter(int id, CharacterDTO characterDTO)
        {
            var characterFromDb = await _context.Characters.FindAsync(id);
            if (characterFromDb == null) return false;

            _context.Entry(characterFromDb).CurrentValues.SetValues(characterDTO);
            int affectedRows = await _context.SaveChangesAsync();
            return affectedRows == 1;
        }

        /// <summary>
        /// Posts a character to the database.
        /// </summary>
        /// <param name="characterToPost">Character to be added</param>
        /// <returns>
        /// If the post failed, <see langword="False"/> is returned.
        /// If the character post succeeded, <see langword="True"/> is returned.
        /// </returns>
        public async Task<bool> PostCharacter(Character characterToPost)
        {
            if (_context.Characters == null) return false;

            await _context.Characters.AddAsync(characterToPost);
            int affectedRows = await _context.SaveChangesAsync();
            return affectedRows == 1;
        }

        /// <summary>
        /// Deletes and removes a character from the database.
        /// </summary>
        /// <param name="id">Id of the character to be deleted</param>
        /// <returns>
        /// If the character was not found, <see langword="False"/> is returned.
        /// If the character was deleted, <see langword="True"/> is returned.
        /// </returns>
        public async Task<bool> DeleteCharacter(int id)
        {
            if (_context.Characters == null) return false;
            var characterFromDb = await _context.Characters.FindAsync(id);
            if (characterFromDb == null) return false;
            _context.Characters.Remove(characterFromDb);

            int affectedRows = await _context.SaveChangesAsync();
            return affectedRows == 1;
        }
    }
}
