﻿using Microsoft.AspNetCore.Mvc;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Repositories
{
    public interface ICharacterRepository
    {
        Task<IEnumerable<Character>> GetCharacters();
        Task<Character>? GetCharacterById(int id);
        Task<bool> PutCharacter(int id, CharacterDTO characterDTO);
        Task<bool> PostCharacter(Character character);
        Task<bool> DeleteCharacter(int id);
    }
}
