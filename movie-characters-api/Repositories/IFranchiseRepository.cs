﻿using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Repositories
{
    public interface IFranchiseRepository
    {
        Task<IEnumerable<Franchise>> GetFranchises();
        Task<Franchise?> GetFranchiseById(int id);
        Task<IEnumerable<Character>?> GetCharactersInFranchise(int id);
        Task<IEnumerable<Movie>?> GetMoviesInFranchise(int id);
        Task<bool> PutFranchise(int id, FranchiseDTO franchiseDTO);
        Task<Franchise?> PutFranchiseMovies(int id, int[] movieIds);
        Task<bool> PostFranchise(Franchise franchise);
        Task<bool> DeleteFranchise(int id);
    }
}
