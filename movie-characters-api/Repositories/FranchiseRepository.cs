﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Repositories
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MovieCharactersDbContext _context;
        public FranchiseRepository(MovieCharactersDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all franchises.
        /// </summary>
        /// <returns>A list of franchises <see cref="List{Franchise}" /></returns>
        public async Task<IEnumerable<Franchise>> GetFranchises()
        {
            return await _context.Franchises.ToListAsync();
        }

        /// <summary>
        /// Gets a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to get.</param>
        /// <returns>A franchise entity or <see langword="null"/>.</returns>
        public async Task<Franchise?> GetFranchiseById(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        /// <summary>
        /// Gets all characters from a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to get characters from.</param>
        /// <returns>A <see cref="List{Character}"/> of characters or <see langword="null"/>.</returns>
        public async Task<IEnumerable<Character>?> GetCharactersInFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null) return null;

            var movies = _context.Movies.Where(m => m.FranchiseId == id);
            return movies.SelectMany(m => m.Characters).Distinct().ToList();
        }

        /// <summary>
        /// Gets all movies from a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to get movies from.</param>
        /// <returns>A <see cref="List{Movie}"/> of movies or <see langword="null"/>.</returns>
        public async Task<IEnumerable<Movie>?> GetMoviesInFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null) return null;

            return await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
        }

        /// <summary>
        /// Creates a new franchise.
        /// </summary>
        /// <param name="franchise">The franchise to create.</param>
        /// <returns><see langword="True"/> if successfully created otherwise returns <see langword="False"/>.</returns>
        public async Task<bool> PostFranchise(Franchise franchise)
        {
            await _context.Franchises.AddAsync(franchise);
            int affectedRows = await _context.SaveChangesAsync();
            return affectedRows == 1;
        }

        /// <summary>
        /// Updates a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to update.</param>
        /// <param name="franchiseDTO">A franchise DTO that contains new values.</param>
        /// <returns><see langword="True"/> if successfully updated otherwise returns <see langword="False"/>.</returns>
        public async Task<bool> PutFranchise(int id, FranchiseDTO franchiseDTO)
        {
            var franchiseFromDb = await _context.Franchises.FindAsync(id);
            if (franchiseFromDb == null) return false;

            _context.Entry(franchiseFromDb).CurrentValues.SetValues(franchiseDTO);
            int affectedRows = await _context.SaveChangesAsync();

            return affectedRows == 1;
        }

        /// <summary>
        /// Updates the list of movies of a specific franchise.
        /// </summary>
        /// <param name="id">Id of franchise to update.</param>
        /// <param name="movieIds">A list of movie ids.</param>
        /// <returns>A <see cref="Franchise"/> entity or <see langword="null"/>.</returns>
        public async Task<Franchise?> PutFranchiseMovies(int id, int[] movieIds)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).Where(f => f.Id == id).FirstOrDefaultAsync();
            if (franchise == null) return null;
            var movieList = franchise.Movies;

            for (int i = 0; i < movieIds.Length; i++)
            {
                var movie = await _context.Movies.FindAsync(movieIds[i]);

                if (movie == null)
                {
                    return null;
                }
                else if (movie != null && !movieList.Contains(movie))
                {
                    movieList.Add(movie);
                }
            }
            return franchise;
        }
        /// <summary>
        /// Deletes a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to delete.</param>
        /// <returns><see langword="True"/> if successfully deleted otherwise, returns <see langword="False"/>.</returns>
        public async Task<bool> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null) return false;

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
