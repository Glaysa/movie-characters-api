using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using movie_characters_api;
using movie_characters_api.Profiles;
using movie_characters_api.Repositories;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(s =>
{
    s.SwaggerDoc("v1",
        new OpenApiInfo
        {
            Title = "Movie Characters API",
            Version = "v1",
            Description = "A simple API to query data about movies, characters and franchises.",
            TermsOfService = new Uri("https://developers.google.com/terms"),
            Contact = new OpenApiContact
            {
                Name = "Scott Sonen and Glaysa Fernandez",
                Email = string.Empty,
                Url = new Uri("https://gitlab.com/Glaysa/movie-characters-api"),
            },
            License = new OpenApiLicense
            {
                Name = "Use under MIT",
                Url = new Uri("https://choosealicense.com/licenses/mit/"),
            },
        });
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFilename);
    s.IncludeXmlComments(xmlPath);
});

builder.Services.AddAutoMapper(typeof(MovieProfile));
builder.Services.AddScoped<ICharacterRepository, CharacterRepository>();
builder.Services.AddScoped<IFranchiseRepository, FranchiseRepository>();
builder.Services.AddScoped<IMovieRepository, MovieRepository>();

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<MovieCharactersDbContext>(options => options.UseSqlServer(connectionString));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
