﻿using AutoMapper;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseDTO, Franchise>().ReverseMap();
        }
    }
}
