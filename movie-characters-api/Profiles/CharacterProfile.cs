﻿using AutoMapper;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<CharacterDTO, Character>().ReverseMap();
        }
    }
}
