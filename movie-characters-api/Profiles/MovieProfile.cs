﻿using AutoMapper;
using movie_characters_api.DTOs;
using movie_characters_api.Models;

namespace movie_characters_api.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<MovieDTO, Movie>().ReverseMap();
        }
    }
}
