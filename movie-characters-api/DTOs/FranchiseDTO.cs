﻿using movie_characters_api.Models;
using System.ComponentModel.DataAnnotations;

namespace movie_characters_api.DTOs
{
    public class FranchiseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
