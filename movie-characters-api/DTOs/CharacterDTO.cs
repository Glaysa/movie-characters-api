﻿using movie_characters_api.Models;
using System.ComponentModel.DataAnnotations;

namespace movie_characters_api.DTOs
{
    public class CharacterDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; }
        public string? Picture { get; set; }
    }
}
