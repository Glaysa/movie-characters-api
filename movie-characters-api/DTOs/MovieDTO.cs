﻿using movie_characters_api.Models;
using System.ComponentModel.DataAnnotations;

namespace movie_characters_api.DTOs
{
    public class MovieDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int? ReleaseYear { get; set; }
        public string? Director { get; set; }
        public string? Trailer { get; set; }
        public int? FranchiseId { get; set; }
    }
}
