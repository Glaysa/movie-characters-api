﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_characters_api;
using movie_characters_api.DTOs;
using movie_characters_api.Models;
using movie_characters_api.Repositories;

namespace movie_characters_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchiseController : ControllerBase
    {
        private readonly IFranchiseRepository _repo;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all franchises.
        /// </summary>
        /// <returns>Status code 200 and a list of franchises.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            var franchises = await _repo.GetFranchises();
            var franchiseDTOs = _mapper.Map<IEnumerable<FranchiseDTO>>(franchises);
            return Ok(franchiseDTOs);
        }

        /// <summary>
        /// Gets a specific franchise by id.
        /// </summary>
        /// <param name="id">Id of the franchise to retrieve.</param>
        /// <returns>
        /// If franchise exists, returns status code 200 Ok and a franchise object
        /// otherwise, returns status code 404 NotFound.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchiseById(int id)
        {
            var franchise = await _repo.GetFranchiseById(id);
            var franchisesDTO = _mapper.Map<FranchiseDTO>(franchise);
            return (franchisesDTO == null) ? NotFound() : Ok(franchisesDTO);
        }

        /// <summary>
        /// Gets all characters from a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>
        /// If franchise exists, returns status code 200 Ok and a list of characters
        /// otherwise, returns status code 404 NotFound.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersInFranchise(int id)
        {
            var characters = await _repo.GetCharactersInFranchise(id);
            var characterDTOs = _mapper.Map<IEnumerable<CharacterDTO>>(characters);
            return (characters == null) ? NotFound() : Ok(characterDTOs);
        }

        /// <summary>
        /// Gets all movies from a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>
        /// If franchise exists, returns status code 200 Ok and a list of movies
        /// otherwise, returns status code 404 NotFound.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMoviesInFranchise(int id)
        {
            var movies = await _repo.GetMoviesInFranchise(id);
            var movieDTOs = _mapper.Map<IEnumerable<MovieDTO>>(movies);
            return (movies == null) ? NotFound() : Ok(movieDTOs);
        }

        /// <summary>
        /// Creates a new franchise.
        /// </summary>
        /// <param name="franchiseDTO">A franchise DTO.</param>
        /// <returns>
        /// If franchise is created successfully, returns status code 200 Ok and the franchise object
        /// otherwise, returns status code 400 BadRequest or 404 NotFound.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<FranchiseDTO>> PostFranchise(FranchiseDTO franchiseDTO)
        {
            var franchiseToPost = _mapper.Map<Franchise>(franchiseDTO);
            var franchiseSaved = await _repo.PostFranchise(franchiseToPost);
            return (!franchiseSaved)
                ? BadRequest(new { Message = "Failed to save franchise." })
                : CreatedAtAction("GetFranchiseById", new { id = franchiseToPost.Id }, franchiseToPost);
        }

        /// <summary>
        /// Updates a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to update.</param>
        /// <param name="franchiseDTO">A franchise DTO that contains new values.</param>
        /// <returns>
        /// If updated successfully, returns a status code 204 NoContent 
        /// otherwise returns a status code 400 BadRequest.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseDTO franchiseDTO)
        {
            var franchiseUpdated = await _repo.PutFranchise(id, franchiseDTO);
            return (!franchiseUpdated) ? BadRequest() : NoContent();
        }

        /// <summary>
        /// Updtes the list of movies of a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to update.</param>
        /// <param name="movieIds">A list of movie ids.</param>
        /// <returns>
        /// If updated successfully, returns a status code 204 NoContent 
        /// otherwise returns a status code 400 BadRequest or 404 NotFound.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> PutFranchiseMovies(int id, int[] movieIds)
        {
            var franchiseUpdated = await _repo.PutFranchiseMovies(id, movieIds);
            return (franchiseUpdated == null)
                ? NotFound()
                : await PutFranchise(id, _mapper.Map<FranchiseDTO>(franchiseUpdated));
        }

        /// <summary>
        /// Deletes a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise to delete.</param>
        /// <returns>
        /// If deleted successfully, returns a status code 204 NoContent 
        /// otherwise returns a status code 404 NotFound.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchiseDeleted = await _repo.DeleteFranchise(id);
            return (!franchiseDeleted) ? NotFound() : NoContent();
        }
    }
}
