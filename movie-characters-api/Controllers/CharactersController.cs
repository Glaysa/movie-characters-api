﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_characters_api.Models;
using movie_characters_api.DTOs;
using movie_characters_api.Repositories;
using System.Net.Mime;

namespace movie_characters_api.Controllers
{
    [ApiController]
    [Route("v1/api/[controller]")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterRepository _repo;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters from database.
        /// </summary>
        /// <returns>All characters from Characters table in database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacters()
        {
            var characters = await _repo.GetCharacters();
            var characterDTOs = _mapper.Map<IEnumerable<CharacterDTO>>(characters);
            return Ok(characterDTOs);
        }

        /// <summary>
        /// Gets specific character by id.
        /// </summary>
        /// <param name="id">Id of the specific character</param>
        /// <returns>
        /// If the character was not found, a 404 Not Found status is returned.
        /// If the character was found, the character object is returned with 200 OK status.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacterById(int id)
        {
            var characterDTO = _mapper.Map<CharacterDTO>(await _repo.GetCharacterById(id));
            return (characterDTO == null) ? NotFound(new { Message = "Character not found." }) : Ok(characterDTO);
        }

        /// <summary>
        /// Updates a character in the database with new values.
        /// </summary>
        /// <param name="id">Id of the character to be updated</param>
        /// <param name="characterDTO">DTO of the character object with new values</param>
        /// <returns>
        /// If the update failed, a 400 Bad Request status is returned.
        /// If the character was updated, a 204 No Content status is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterDTO characterDTO)
        {
            if (id != characterDTO.Id)
            {
                return BadRequest();
            }

            var isUpdated = await _repo.PutCharacter(id, characterDTO);

            if (isUpdated) return NoContent();
            else throw new DbUpdateConcurrencyException();
        }

        /// <summary>
        /// Posts a character to the database.
        /// </summary>
        /// <param name="characterDTO">DTO of the character to be added</param>
        /// <returns>
        /// If the post failed, a 400 Bad Request or 404 Not Found status is returned.
        /// If the character post succeeded, a 201 Created or 200 OK is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<CharacterDTO>> PostCharacter(CharacterDTO characterDTO)
        {
            var characterToPost = _mapper.Map<Character>(characterDTO);
            var isCharacterPosted = await _repo.PostCharacter(characterToPost);
            return (!isCharacterPosted)
                ? BadRequest()
                : CreatedAtAction("GetCharacter", new { id = characterToPost.Id }, characterToPost);
        }

        /// <summary>
        /// Deletes and removes a character from the database.
        /// </summary>
        /// <param name="id">Id of the character to be deleted</param>
        /// <returns>
        /// If the character was not deleted, a 404 Not Found status is returned.
        /// If the character was deleted, a 204 No Content status is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var isUpdated = await _repo.DeleteCharacter(id);
            return (!isUpdated) ? NotFound() : NoContent();
        }
    }
}
