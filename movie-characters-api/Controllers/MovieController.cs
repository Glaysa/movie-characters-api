﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_characters_api.DTOs;
using movie_characters_api.Models;
using movie_characters_api.Repositories;
using System.Net.Mime;

namespace movie_characters_api.Controllers
{
    [ApiController]
    [Route("v1/api/[controller]")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MovieController : ControllerBase
    {
        private readonly IMovieRepository _repo;
        private readonly IMapper _mapper;

        public MovieController(IMovieRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies from database.
        /// </summary>
        /// <returns>All movies from Movies table in database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            var movies = await _repo.GetMovies();
            var movieDTOs = _mapper.Map<IEnumerable<MovieDTO>>(movies);
            return Ok(movieDTOs);
        }

        /// <summary>
        /// Gets all characters from specific movie by id.
        /// </summary>
        /// <param name="id">Id of the specific movie to check</param>
        /// <returns>
        /// If the movie or the characters were not found, a 404 Not Found status is returned.
        /// If the characters were found, the list is returned with 200 OK status.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersInMovie(int id)
        {
            var characters = await _repo.GetCharactersInMovie(id);
            var characterDTOs = _mapper.Map<IEnumerable<CharacterDTO>>(characters);
            return (characters == null) ? NotFound() : Ok(characterDTOs);
        }

        /// <summary>
        /// Gets specific movie by id.
        /// </summary>
        /// <param name="id">Id of the specific movie</param>
        /// <returns>
        /// If the movie was not found, a 404 Not Found status is returned.
        /// If the movie was found, the movie object is returned with 200 OK status.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovieById(int id)
        {
            var movie = await _repo.GetMovieById(id);
            var movieDTO = _mapper.Map<MovieDTO>(movie);
            return (movieDTO == null) ? NotFound(new { Message = "Movie not found." }) : Ok(movieDTO);
        }

        /// <summary>
        /// Posts a movie to the database.
        /// </summary>
        /// <param name="movieDTO">DTO of the movie to be added</param>
        /// <returns>
        /// If the post failed, a 400 Bad Request or 404 Not Found status is returned.
        /// If the movie post succeeded, a 201 Created or 200 OK is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<MovieDTO>> PostMovie(MovieDTO movieDTO)
        {
            var movieToPost = _mapper.Map<Movie>(movieDTO);
            var movieSaved = await _repo.PostMovie(movieToPost);
            return (!movieSaved)
                ? BadRequest(new { Message = "Failed to save movie." })
                : CreatedAtAction("GetMovie", new { id = movieToPost.Id }, movieToPost);
        }

        /// <summary>
        /// Updates a movie in the database with new values.
        /// </summary>
        /// <param name="id">Id of the movie to be updated</param>
        /// <param name="newMovie">DTO of the movie object with new values</param>
        /// <returns>
        /// If the update failed, a 400 Bad Request status is returned.
        /// If the movie was updated, a 204 No Content status is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieDTO newMovie)
        {
            var movieUpdated = await _repo.PutMovie(id, newMovie);
            return (!movieUpdated) ? BadRequest() : NoContent();
        }

        /// <summary>
        /// Updates the characters list of a movie.
        /// </summary>
        /// <param name="id">Id of the movie to be updated</param>
        /// <param name="characterIds">List of Ids of the characters to be added to the list</param>
        /// <returns>
        /// If the movie with Id was not found, a 404 Not Found status is returned.
        /// If the update failed, a 400 Bad Request status is returned.
        /// If the movie was updated, a 204 No Content status is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> PutMovieCharacters(int id, int[] characterIds)
        {
            var movieUpdated = await _repo.PutMovieCharacters(id, characterIds);
            return (movieUpdated == null)
                ? NotFound()
                : await PutMovie(id, _mapper.Map<MovieDTO>(movieUpdated));
        }

        /// <summary>
        /// Deletes and removes a movie from the database.
        /// </summary>
        /// <param name="id">Id of the movie to be deleted</param>
        /// <returns>
        /// If the movie was not deleted, a 404 Not Found status is returned.
        /// If the movie was deleted, a 204 No Content status is returned.
        /// </returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movieDeleted = await _repo.DeleteMovie(id);
            return (!movieDeleted) ? NotFound() : NoContent();
        }
    }
}
