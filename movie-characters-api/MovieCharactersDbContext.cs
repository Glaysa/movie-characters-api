﻿using Microsoft.EntityFrameworkCore;
using movie_characters_api.Models;
using movie_characters_api.DataAccess;

namespace movie_characters_api
{
    public class MovieCharactersDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharactersDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seeds data to the database with the help of the SeedDataHelper class.

            modelBuilder.Entity<Character>().HasData(SeedDataHelper.GetCharacters());
            modelBuilder.Entity<Movie>().HasData(SeedDataHelper.GetMovies());
            modelBuilder.Entity<Franchise>().HasData(SeedDataHelper.GetFranchises());
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity(j => j.HasData(
                    new { CharactersId = 1, MoviesId = 1 },
                    new { CharactersId = 1, MoviesId = 2 },
                    new { CharactersId = 2, MoviesId = 1 },
                    new { CharactersId = 2, MoviesId = 2 },
                    new { CharactersId = 3, MoviesId = 1 },
                    new { CharactersId = 3, MoviesId = 2 }
                ));
        }
    }
}
